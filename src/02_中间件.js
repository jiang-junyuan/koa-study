//1、导入Koa
const Koa = require('koa')
//2、实例化app对象
const app = new Koa()
//3、编写中间件
app.use((ctx,next) => {
    console.log("我来组成头部")
    next()
})
app.use((ctx,next) => {
    console.log('我来组成身体')
    next()
})
app.use((ctx) => {
    console.log('组装完成')
    ctx.body = '组装完成'
})
//4、启动服务
app.listen(3000,() => {
    console.log('server is running on http://localhost:3000')
})
