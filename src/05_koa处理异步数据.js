const Koa = require('koa')
const app = new Koa()
app.use(async (ctx,next) => {
    ctx.message = 'aa'
    await next()
    ctx.body = ctx.message
})
app.use(async (ctx,next) => {
    ctx.message += 'bb'
    await next()
})
app.use((ctx) => {
    Promise.resolve('cc').then(data => {
        ctx.message += 'cc'
    })
})
app.listen(3000,() => {
    console.log('server is running on http://localhost:3000')
})
