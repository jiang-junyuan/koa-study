const Koa = require('koa')
const app = new Koa()
app.use((ctx,next) => {
    ctx.message = 'aa'
    next()
    ctx.body = ctx.message
})
app.use((ctx,next) => {
    ctx.message += 'bb'
    next()
})
app.use((ctx) => {
    ctx.message += 'cc'
})
app.listen(3000,() => {
    console.log('server is running on http://localhost:3000')
})
