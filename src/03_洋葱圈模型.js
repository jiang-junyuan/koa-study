const Koa = require("koa")
const app = new Koa()
app.use((ctx,next) => {
    console.log('1')
    next()
    console.log('2')
})
app.use((ctx,next) => {
    console.log('3')
    next()
    console.log('4')
})
app.use((ctx) => {
    console.log('5')
    ctx.body = '处理完成'
})
app.listen(3000,()=>{
    console.log('server is running on http://localhost:3000')
})

//输出：13542
