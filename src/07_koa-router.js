const Koa = require('koa')
const app = new Koa

// 注册koa-body中间件，解析请求体中的参数，挂载到ctx.request.body上
const KoaBody = require('koa-body')
app.use(KoaBody())

//导入router
const userRouter = require('./router/user.router')

//注册中间件
app.use(userRouter.routes()).use(userRouter.allowedMethods())//没配置的请求报405/501错误

// 监听错误事件
app.on('error',(err,ctx)=>{
    console.log(err)
    ctx.body = err
})

app.listen(3000,()=>{
    console.log('server is running on http://localhost:3000')
})
